
let students = ["John", "Joe", "Jane", "Jessie"]

//1.
const addToEnd = (arr, element) => {
	if(typeof element !== "string"){
		console.log("Error - Can only add strings to the array.")
	}else{
		arr.push(element)
		console.log(arr)
	}
}

//test cases:
// addToEnd(students, "Ryan")
// addToEnd(students, 045)

//2.

const addToStart = (arr, element) => {
	if(typeof element !== "string"){
		console.log("Error - Can only add strings to the array.")
	}else{
		arr.unshift(element)
		console.log(arr)
	}
}

//test cases:
// addToStart(students, "Tess")
// addToStart(students, 033)

//3.
const elementChecker = (arr, elementToCheck) => {
	if(arr.length === 0){
		console.log("Error - Passed in array is empty")
	}else{
		let result = arr.some(element => element === elementToCheck)
		console.log(result)
	}
}

//test cases:
// elementChecker(students, "Jane")
// elementChecker([], "Jane")

//4.

const checkAllStringsEnding = (arr, char) => {
	if(arr.length === 0){
		console.log("Error - Passed in array is empty")
	}else if(arr.some(element => typeof element !== "string")){
		console.log("Error - All array elements must be strings")
	}else if(char.length > 1){
		console.log("Error - Second argument must be a single character")
	}else{
		let result = arr.every(element => element[element.length-1] === char)
		console.log(result)
	}
}

//test cases:
// checkAllStringsEnding(students, "e")
// checkAllStringsEnding([], "e")
// checkAllStringsEnding(["Jane", 02], "e")

//5.
const stringLengthSorter = (arr) => {
	if(arr.some(element => typeof element !== "string")){
		console.log("Error - All array elements must be strings")
	}else{
		arr.sort((elementA, elementB) => {
			return elementA.length - elementB.length
		})

		console.log(arr)
	}
}

//test cases:
// stringLengthSorter(students)
// stringLengthSorter([037, "John", 039, "Jane"])

//6.
const startsWithCounter = (arr, char) => {
	if(arr.length === 0){
		console.log("Error - Passed in array is empty")
	}else if(arr.some(element => typeof element !== "string")){
		console.log("Error - All array elements must be strings")
	}else if(char.length > 1){
		console.log("Error - Second argument must be a single character")
	}else{
		let result = 0

		arr.forEach(element => {
			if(element[0].toLowerCase() === char.toLowerCase()){
				result++
			}
		})

		console.log(result)
	}
}

//test cases:
// startsWithCounter(students, "j")

const likeFinder = (arr, str) => {
if(arr.length === 0){
		console.log("Error - Passed in array is empty")
	}else if(arr.some(element => typeof element !== "string")){
		console.log("Error - All array elements must be strings")
	}else if(typeof str !== "string"){
		console.log("Error - Second argument must be of data type string")
	}else{
		let result = []

		arr.forEach(element => {
			if(element.toLowerCase().includes(str.toLowerCase())){
				result.push(element)
			}
		})

		console.log(result)
	}	
}

//test cases:
// likeFinder(students, "jo")

//8.
const randomPicker = (arr) => {
	if(arr.length === 0){
		console.log("Error - Passed in array is empty")
	}else{
		console.log(arr[Math.floor(Math.random() * arr.length)])
	}
}

//test cases
randomPicker(students)

